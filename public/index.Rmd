---
pagetitle: "Task clase-04"
output: 
 html_document:
   self_contained: FALSE
---
# Task Clase-04

Escriba las respuestas del task en un script de R. Posteriormente cree una carpeta llamada clase-04 en el repositorio de GitHub creado en la clase-02. Guarde el script en la carpeta clase-04 y suba (push) el script al repositorio de github.

<!--Esto no se modifica-->
```{r setup, include=FALSE}
rm(list=ls())
library(pacman)
p_load(tutorial,tidyverse,testthat)
tutorial::go_interactive(height = 800)
```

<!--Esto no se toca a menos que quieras llamar una libreria nueva sino solo deja tidyverse-->
```{r ex="create_a", type="pre-exercise-code"}
library(dplyr)
library(tidyr)

```

<!--Escribir quí las preguntas del task-->
```{r ex="create_a", type="sample-code"}

# Punto 1: llame a la base de datos mtcars y asigne esta base de datos a un objeto llamado cars. En esta base de datos se debe añadir una columna extra (llamada id) enumerando la cantidad de filas dentro de la base de datos. 


# Punto 2: Utilice la columna qsec para crear una variable llamada velocidad, la cual deberá ser igual a "rapido" si la velocidad es menor a 15, "ultra rapido" si es menor a 20, "rapido y furioso" si esta entre 20 y 22.89, y, "Not even competing" si es igual a 22.90. 


# Punto 3: en un nuevo objeto llamado code, seleccione las columnas de (vs, am, gear, carb, id, velocidad) y junte las primeras 4 columnas dentro de una columna llamada codigo, separando los valores con un -. Por ultimo, excluya  las 4 columnas que se usaron para elaborar la columna codigo. (EJ de columna código: vs-am-gear-carb)


# Punto 4: Convierta todos los nombres de las filas a minúscula.


# Punto 5: Si se realizaron todos los pasos correctamente, debería de haber un na dentro de la base de datos, porfavor remueva la fila en el cual este na se encuentra. 


```

<!--Escribir la respuesta del task-->
```{r ex="create_a", type="solution"}
# Punto 1:  

cars = mtcars %>% mutate(id = 1:nrow(mtcars))

# Punto 2: 
cars = mutate(cars , velocidad  = case_when(cars$qsec < 15 ~ "rapido" ,
                                            cars$qsec < 20 ~ "ultra rapido" ,
                                            cars$qsec > 20 & cars$qsec < 22.90 ~ "rapido y furioso",
                                            cars$qsec == 22.90 ~ "Not even competing"))

# Punto 3: 
code = cars %>% 
  select( vs, am, gear, carb, id, velocidad) %>%
  mutate(codigo = paste( vs, am, gear, carb, sep = "-")) %>%
  select(-c(vs, am, gear, carb))

# Punto 4: 
rownames(code) = tolower(rownames(code))

# Punto 5: 
code = drop_na(code)

```

<!--Esto no se toca-->
```{r ex="create_a", type="sct"}
ex() %>% check_object("code") %>% check_equal()
success_msg("Well done!")
```

